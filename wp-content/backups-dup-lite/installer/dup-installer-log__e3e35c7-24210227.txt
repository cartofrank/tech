********************************************************************************
* DUPLICATOR-PRO: Install-Log
* STEP-1 START @ 06:45:24
* VERSION: 1.3.36
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
PACKAGE INFO________ CURRENT SERVER                         |ORIGINAL SERVER
PHP VERSION_________: 7.2.31                                |7.3.10
OS__________________: WINNT                                 |WINNT
CREATED_____________: 2020-08-24 21:02:27
WP VERSION__________: 5.5
DUP VERSION_________: 1.3.36
DB__________________: 10.4.11
DB TABLES___________: 58
DB ROWS_____________: 1,927
DB FILE SIZE________: 7.39MB
********************************************************************************
SERVER INFO
PHP_________________: 7.3.10 | SAPI: apache2handler
PHP MEMORY__________: 4294967296 | SUHOSIN: disabled
SERVER______________: Apache/2.4.41 (Win64) OpenSSL/1.1.1c PHP/7.3.10
DOC ROOT____________: "C:/xampp/htdocs/desarrollo/technerds"
DOC ROOT 755________: true
LOG FILE 644________: true
REQUEST URL_________: "http://localhost/desarrollo/technerds/dup-installer/main.installer.php"
********************************************************************************
USER INPUTS
ARCHIVE ENGINE______: "ziparchive"
SET DIR PERMS_______: 0
DIR PERMS VALUE_____: 644
SET FILE PERMS______: 0
FILE PERMS VALUE____: 755
SAFE MODE___________: "0"
LOGGING_____________: "1"
CONFIG MODE_________: "NEW"
FILE TIME___________: "current"
********************************************************************************


--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "C:/xampp/htdocs/desarrollo/technerds/20200821_tbltechnerdsshop_[HASH]_20200824210227_archive.zip"
SIZE________________: 66.11MB

PRE-EXTRACT-CHECKS
- PASS: Apache '.htaccess' not found - no backup needed.
- PASS: Microsoft IIS 'web.config' not found - no backup needed.
- PASS: WordFence '.user.ini' not found - no backup needed.


START ZIP FILE EXTRACTION STANDARD >>> 

--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "C:/xampp/htdocs/desarrollo/technerds/20200821_tbltechnerdsshop_[HASH]_20200824210227_archive.zip"
SIZE________________: 66.11MBFile timestamp set to Current: 2020-08-25 19:03:45
<<< ZipArchive Unzip Complete: true
--------------------------------------
POST-EXTACT-CHECKS
--------------------------------------

PERMISSION UPDATES: None Applied

STEP-1 COMPLETE @ 07:03:45 - RUNTIME: 1100.8751 sec.



********************************************************************************
* DUPLICATOR-LITE INSTALL-LOG
* STEP-2 START @ 07:19:42
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
USER INPUTS
VIEW MODE___________: "basic"
DB ACTION___________: "empty"
DB HOST_____________: "**OBSCURED**"
DB NAME_____________: "**OBSCURED**"
DB PASS_____________: "**OBSCURED**"
DB PORT_____________: "**OBSCURED**"
NON-BREAKING SPACES_: false
MYSQL MODE__________: "DEFAULT"
MYSQL MODE OPTS_____: ""
CHARSET_____________: "utf8"
COLLATE_____________: "utf8_general_ci"
COLLATE FB__________: false
VIEW CREATION_______: true
STORED PROCEDURE____: true
********************************************************************************

--------------------------------------
DATABASE-ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 10.4.8 -- Build Server: 10.4.11
FILE SIZE:	dup-database__[HASH].sql (2.79MB)
TIMEOUT:	5000
MAXPACK:	1048576
SQLMODE:	NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION
NEW SQL FILE:	[C:/xampp/htdocs/desarrollo/technerds/dup-installer/dup-installer-data__[HASH].sql]
COLLATE FB:	Off

NOTICE: This servers version [10.4.8] is less than the build version [10.4.11].  
If you find issues after testing your site please referr to this FAQ item.
https://snapcreek.com/duplicator/docs/faqs-tech/#faq-installer-260-q
[PHP ERR][WARN] MSG:count(): Parameter must be an array or an object that implements Countable [CODE:2|FILE:C:\xampp\htdocs\desarrollo\technerds\dup-installer\ctrls\ctrl.s2.dbinstall.php|LINE:392]
--------------------------------------
DATABASE RESULTS
--------------------------------------
DB VIEWS:	enabled
DB PROCEDURES:	enabled
ERRORS FOUND:	0
DROPPED TABLES:	0
RENAMED TABLES:	0
QUERIES RAN:	503

wp_actionscheduler_actions: (11)
wp_actionscheduler_claims: (0)
wp_actionscheduler_groups: (3)
wp_actionscheduler_logs: (33)
wp_commentmeta: (0)
wp_comments: (1)
wp_duplicator_packages: (0)
wp_links: (0)
wp_options: (356)
wp_postmeta: (893)
wp_posts: (183)
wp_revslider_css: (109)
wp_revslider_css_bkp: (47)
wp_revslider_layer_animations: (0)
wp_revslider_layer_animations_bkp: (0)
wp_revslider_navigations: (0)
wp_revslider_navigations_bkp: (0)
wp_revslider_sliders: (1)
wp_revslider_sliders_bkp: (0)
wp_revslider_slides: (3)
wp_revslider_slides_bkp: (0)
wp_revslider_static_slides: (1)
wp_revslider_static_slides_bkp: (0)
wp_term_relationships: (55)
wp_term_taxonomy: (51)
wp_termmeta: (28)
wp_terms: (51)
wp_usermeta: (26)
wp_users: (1)
wp_wc_admin_note_actions: (12)
wp_wc_admin_notes: (11)
wp_wc_category_lookup: (1)
wp_wc_customer_lookup: (0)
wp_wc_download_log: (0)
wp_wc_order_coupon_lookup: (0)
wp_wc_order_product_lookup: (0)
wp_wc_order_stats: (0)
wp_wc_order_tax_lookup: (0)
wp_wc_product_meta_lookup: (6)
wp_wc_reserved_stock: (0)
wp_wc_tax_rate_classes: (4)
wp_wc_webhooks: (0)
wp_woocommerce_api_keys: (0)
wp_woocommerce_attribute_taxonomies: (2)
wp_woocommerce_downloadable_product_permissions: (0)
wp_woocommerce_log: (0)
wp_woocommerce_order_itemmeta: (0)
wp_woocommerce_order_items: (0)
wp_woocommerce_payment_tokenmeta: (0)
wp_woocommerce_payment_tokens: (0)
wp_woocommerce_sessions: (1)
wp_woocommerce_shipping_zone_locations: (0)
wp_woocommerce_shipping_zone_methods: (0)
wp_woocommerce_shipping_zones: (0)
wp_woocommerce_tax_rate_locations: (0)
wp_woocommerce_tax_rates: (0)
wp_woodmart_wishlist_products: (0)
wp_woodmart_wishlists: (2)
Removed '93' cache/transient rows

INSERT DATA RUNTIME: 55.3747 sec.
STEP-2 COMPLETE @ 07:20:38 - RUNTIME: 55.5379 sec.

====================================
SET SEARCH AND REPLACE LIST
====================================


********************************************************************************
DUPLICATOR PRO INSTALL-LOG
STEP-3 START @ 07:21:01
NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	"utf8"
CHARSET CLIENT:	"utf8"
********************************************************************************
OPTIONS:
blogname______________: "TBL TECHNERDS SHOP"
postguid______________: false
fullsearch____________: false
path_old______________: "C:/xampp/htdocs/desarrollo/technerds"
path_new______________: "C:/xampp/htdocs/desarrollo/technerds"
siteurl_______________: "http://localhost/desarrollo/technerds"
url_old_______________: "http://localhost/desarrollo/technerds"
url_new_______________: "http://localhost/desarrollo/technerds"
maxSerializeStrlen____: 4000000
replaceMail___________: false
dbcharset_____________: "utf8"
dbcollate_____________: "utf8_general_ci"
wp_mail_______________: ""
wp_nickname___________: ""
wp_first_name_________: ""
wp_last_name__________: ""
ssl_admin_____________: false
cache_wp______________: false
cache_path____________: false
exe_safe_mode_________: false
config_mode___________: "NEW"
********************************************************************************


====================================
RUN SEARCH AND REPLACE
====================================

EVALUATE TABLE: "wp_actionscheduler_actions"______________________[ROWS:    11][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_actionscheduler_claims"_______________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_actionscheduler_groups"_______________________[ROWS:     3][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_actionscheduler_logs"_________________________[ROWS:    33][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_commentmeta"__________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_comments"_____________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_duplicator_packages"__________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_links"________________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_options"______________________________________[ROWS:   356][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_postmeta"_____________________________________[ROWS:   893][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_posts"________________________________________[ROWS:   183][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_revslider_css"________________________________[ROWS:   109][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_revslider_css_bkp"____________________________[ROWS:    47][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_revslider_layer_animations"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_revslider_layer_animations_bkp"_______________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_revslider_navigations"________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_revslider_navigations_bkp"____________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_revslider_sliders"____________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_revslider_sliders_bkp"________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_revslider_slides"_____________________________[ROWS:     3][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_revslider_slides_bkp"_________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_revslider_static_slides"______________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_revslider_static_slides_bkp"__________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_term_relationships"___________________________[ROWS:    55][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_term_taxonomy"________________________________[ROWS:    51][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_termmeta"_____________________________________[ROWS:    28][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_terms"________________________________________[ROWS:    51][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_usermeta"_____________________________________[ROWS:    26][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_users"________________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_wc_admin_note_actions"________________________[ROWS:    12][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_wc_admin_notes"_______________________________[ROWS:    11][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_wc_category_lookup"___________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_wc_customer_lookup"___________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wc_download_log"______________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wc_order_coupon_lookup"_______________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wc_order_product_lookup"______________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wc_order_stats"_______________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wc_order_tax_lookup"__________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wc_product_meta_lookup"_______________________[ROWS:     6][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_wc_reserved_stock"____________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wc_tax_rate_classes"__________________________[ROWS:     4][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_wc_webhooks"__________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_api_keys"_________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_attribute_taxonomies"_____________[ROWS:     2][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_woocommerce_downloadable_product_permissions"_[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_log"______________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_order_itemmeta"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_order_items"______________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_payment_tokenmeta"________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_payment_tokens"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_sessions"_________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"

EVALUATE TABLE: "wp_woocommerce_shipping_zone_locations"__________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_shipping_zone_methods"____________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_shipping_zones"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_tax_rate_locations"_______________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_tax_rates"________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woodmart_wishlist_products"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woodmart_wishlists"___________________________[ROWS:     2][PG:   1][SCAN:text columns]
	SEARCH  1:"C:\xampp\htdocs\desarrollo\technerds" ============> "C:/xampp/htdocs/desarrollo/technerds"
	SEARCH  2:"C:\\xampp\\htdocs\\desarrollo\\technerds" ========> "C:\/xampp\/htdocs\/desarrollo\/technerds"
	SEARCH  3:"C%3A%5Cxampp%5Chtdocs%5Cdesarrollo%5Ctechnerds" ==> "C%3A%2Fxampp%2Fhtdocs%2Fdesarrollo%2Ftechnerds"
	SEARCH  4:"https://localhost/desarrollo/technerds" ==========> "http://localhost/desarrollo/technerds"
	SEARCH  5:"https:\/\/localhost\/desarrollo\/technerds" ======> "http:\/\/localhost\/desarrollo\/technerds"
	SEARCH  6:"https%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds" => "http%3A%2F%2Flocalhost%2Fdesarrollo%2Ftechnerds"
--------------------------------------
SCANNED:	Tables:58 	|	 Rows:1892 	|	 Cells:11792 
UPDATED:	Tables:1 	|	 Rows:1 	|	 Cells:1 
ERRORS:		0 
RUNTIME:	8.111900 sec

====================================
REMOVE LICENSE KEY
====================================

====================================
CREATE NEW ADMIN USER
====================================

====================================
CONFIGURATION FILE UPDATES
====================================
	UPDATE DB_NAME ""tech""
	UPDATE DB_USER ""root""
	UPDATE DB_PASSWORD "** OBSCURED **"
	UPDATE DB_HOST ""localhost""
	REMOVE WPCACHEHOME
	
*** UPDATED WP CONFIG FILE ***

====================================
HTACCESS UPDATE MODE: "NEW"
====================================
- PASS: Successfully created a new .htaccess file.
- PASS: Existing Apache '.htaccess__[HASH]' was removed

====================================
GENERAL UPDATES & CLEANUP
====================================

====================================
DEACTIVATE PLUGINS CHECK
====================================
Deactivated plugins list here: Array
(
    [0] => really-simple-ssl/rlrsssl-really-simple-ssl.php
    [1] => js_composer/js_composer.php
)

deactivate js_composer/js_composer.php
Plugin(s) listed here are deactivated: js_composer/js_composer.php
Plugin(s) reactivated after installation: js_composer/js_composer.php

====================================
NOTICES TEST
====================================
No General Notices Found


====================================
CLEANUP TMP FILES
====================================

====================================
FINAL REPORT NOTICES
====================================

STEP-3 COMPLETE @ 07:21:10 - RUNTIME: 9.1396 sec. 


====================================
FINAL REPORT NOTICES LIST
====================================
-----------------------
[INFO] Info
	SECTIONS: general
	LONG MSG:             The following is a list of notices that may need to be fixed in order to finalize your setup. These values should only be investigated if you're running into
            issues with your site. For more details see the <a href="https://codex.wordpress.org/Editing_wp-config.php" target="_blank">WordPress Codex</a>.

-----------------------
[INFO] No errors in database
	SECTIONS: database

-----------------------
[INFO] No search and replace data errors
	SECTIONS: search_replace

-----------------------
[INFO] No files extraction errors
	SECTIONS: files

-----------------------
[WARNING] Deactivated Plugin:  WPBakery Page Builder
	SECTIONS: general
	LONG MSG: This plugin is deactivated automatically, because it requires a reacivation to work properly.  <b>Please reactivate from the WordPress admin panel after logging in.</b> This will re-enable your site's frontend.

====================================
